package com.api_transacao.projeto_api_transacao.services;

import com.api_transacao.projeto_api_transacao.models.Transacao;
import com.api_transacao.projeto_api_transacao.models.Usuario;
import com.api_transacao.projeto_api_transacao.repositories.TransacaoRepository;
import com.api_transacao.projeto_api_transacao.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class TransacaoService {
    @Autowired
    private TransacaoRepository transacaoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Transacao realizarTransacao(Long remetenteId, Long destinatarioId, Double valor) {
        Usuario remetente = usuarioRepository.findById(remetenteId).orElseThrow(() -> new RuntimeException("Remetente não encontrado"));
        Usuario destinatario = usuarioRepository.findById(destinatarioId).orElseThrow(() -> new RuntimeException("Destinatario não encontrado"));

        if (remetente.getSaldo() < valor) {
            throw new RuntimeException("Saldo insuficiente");
        }

        remetente.setSaldo(remetente.getSaldo() - valor);
        destinatario.setSaldo(destinatario.getSaldo() + valor);

        usuarioRepository.save(remetente);
        usuarioRepository.save(destinatario);

        Transacao transacao = new Transacao();
        transacao.setRemetenteId(remetenteId);
        transacao.setDestinatarioId(destinatarioId);
        transacao.setValor(valor);
        transacao.setDataTransacao(LocalDateTime.now());
        transacao.setStatus("COMPLETADA");

        return transacaoRepository.save(transacao);
    }
}

