package com.api_transacao.projeto_api_transacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoApiTransacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoApiTransacaoApplication.class, args);
	}

}
