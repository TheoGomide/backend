package com.api_transacao.projeto_api_transacao.controllers;

import com.api_transacao.projeto_api_transacao.models.Transacao;
import com.api_transacao.projeto_api_transacao.services.TransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transacoes")
public class TransacaoController {
    @Autowired
    private TransacaoService transacaoService;

    @PostMapping()
    public realizarTransacao realizarTransacao(@RequestParam Long remetenteId, @RequestParam Long destinatarioId, @RequestParam Double valor) {
        return transacaoService.realizarTransacao(remetenteId, destinatarioId, valor);
    }

    @GetMapping(value = {"/transacaoId"})
    public buscarTransacao buscarTransacao() {
        return null;
    }

    @PatchMapping(value = {"/transacaoId"})
    public atualizarTransacao atualizarTransacao(@RequestParam string status) {
        return null;
    }
}

