package com.api_transacao.projeto_api_transacao.models;

public class Endereco {
        private String cidade;
        private String bairro;
        private String cep;
        private String numero;
    
        public String getCidade() {
            return cidade;
        }
    
        public void setCidade(String cidade) {
            this.cidade = cidade;
        }
    
        public String getBairro() {
            return bairro;
        }
    
        public void setBairro(String bairro) {
            this.bairro = bairro;
        }
    
        public String getCep() {
            return cep;
        }
    
        public void setCep(String cep) {
            this.cep = cep;
        }
    
        public String getNumero() {
            return numero;
        }
    
        public void setNumero(String numero) {
            this.numero = numero;
        }
    }
