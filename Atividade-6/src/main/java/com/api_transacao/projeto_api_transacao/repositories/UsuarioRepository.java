package com.api_transacao.projeto_api_transacao.repositories;

import com.api_transacao.projeto_api_transacao.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
}
