package com.api_transacao.projeto_api_transacao.repositories;

import com.api_transacao.projeto_api_transacao.models.Transacao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransacaoRepository extends JpaRepository<Transacao, Long> {
}
