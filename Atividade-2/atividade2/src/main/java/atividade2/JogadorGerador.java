package ativiade2;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Random;

public class JogadorGerador {
    private static String nomesURL = "https://venson.net.br/resources/data/nomes.txt";
    private static String sobrenomesURL = "https://venson.net.br/resources/data/sobrenomes.txt";
    private static String posicoesURL = "https://venson.net.br/resources/data/posicoes.txt";
    private static String clubesURL = "https://venson.net.br/resources/data/clubes.txt";

    private static String[] nomes;
    private static String[] sobrenomes;
    private static String[] posicoes;
    private static String[] clubes;

    private static boolean dadosCarregados = false;

    public static void carregarDados() throws Exception {
        if (!dadosCarregados) {
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest nomesRequest = HttpRequest.newBuilder().uri(URI.create(nomesURL)).build();
            HttpRequest sobrenomesRequest = HttpRequest.newBuilder().uri(URI.create(sobrenomesURL)).build();
            HttpRequest posicoesRequest = HttpRequest.newBuilder().uri(URI.create(posicoesURL)).build();
            HttpRequest clubesRequest = HttpRequest.newBuilder().uri(URI.create(clubesURL)).build();

            HttpResponse<String> nomesResponse = client.send(nomesRequest, HttpResponse.BodyHandlers.ofString());
            HttpResponse<String> sobrenomesResponse = client.send(sobrenomesRequest, HttpResponse.BodyHandlers.ofString());
            HttpResponse<String> posicoesResponse = client.send(posicoesRequest, HttpResponse.BodyHandlers.ofString());
            HttpResponse<String> clubesResponse = client.send(clubesRequest, HttpResponse.BodyHandlers.ofString());

            nomes = nomesResponse.body().split("\n");
            sobrenomes = sobrenomesResponse.body().split("\n");
            posicoes = posicoesResponse.body().split("\n");
            clubes = clubesResponse.body().split("\n");

            dadosCarregados = true;
        }
    }

    public static Jogador gerarJogador() throws Exception {
        carregarDados();
        Random random = new Random();
        String nome = nomes[random.nextInt(nomes.length)];
        String sobrenome = sobrenomes[random.nextInt(sobrenomes.length)];
        String posicao = posicoes[random.nextInt(posicoes.length)];
        String clube = clubes[random.nextInt(clubes.length)];
        int idade = gerarNumeroAleatorio();
        return new Jogador(nome, sobrenome, posicao, clube, idade);
    }

    private static int gerarNumeroAleatorio() {
        Random random = new Random();
        return random.nextInt(23) + 17;
    }
}
